<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class Soap {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    public $id;

    /** @ORM\Column(type="string", length=64) */
    public $request;

    /** @ORM\Column(type="string", length=64) */
    public $response;

    /** @ORM\Column(type="datetime") */
    public $date;

    /**
     * Automaticly setting date
     */
    public function __construct() {
        $this->date = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set request
     *
     * @param string $request
     * @return Soap
     */
    public function setRequest($request) {
        $this->request = $request;

        return $this;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return Soap
     */
    public function setResponse($response) {
        $this->response = $response;

        return $this;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Soap
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     * @return type
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * get response
     * @return type
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * Get request
     * @return type
     */
    public function getRequest() {
        return $this->request;
    }

}
