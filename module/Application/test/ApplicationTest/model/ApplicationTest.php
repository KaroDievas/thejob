<?php

namespace ApplicationTest\Model;

use PHPUnit_Framework_TestCase;

class ApplicationTest extends PHPUnit_Framework_TestCase {

    // Doctrine 2 entity manager
    protected $em;
    protected $doctrine;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    public function setUp() {
        $this->em = $this->getMock('EntityManager', array('persist', 'flush'));

        $this->em->expects($this->any())
                ->method('persist')
                ->will($this->returnValue(true));

        $this->em->expects($this->any())
                ->method('flush')
                ->will($this->returnValue(true));

        $this->doctrine = $this->getMock('Doctrine', array('getEntityManager'));

        $this->doctrine
                ->expects($this->any())
                ->method('getEntityManager')
                ->will($this->returnValue($this->em));
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed.
     */
    public function tearDown() {
        $this->doctrine = null;
        $this->em = null;
    }

    public function testApplicationInitialState() {
        $app = new \Application\Entity\soap();

        $this->assertNull($app->response, '"response" should initially be null');
        $this->assertNull($app->id, '"id" should initially be null');
        $this->assertNull($app->request, '"request" should initially be null');
        $this->assertNotNull($app->date, '"date" should initially be null');
    }

    public function testStringReverserReturnExceptionOnEmptyStringRequest() {
        $this->setExpectedException(
                'UnexpectedValueException', 'Empty string value. String value should be not empty!'
        );

        $stringReverser = new \Application\Service\StringReverser();

        $stringReverser->reverseString("");
    }

    public function testStringReverserReturnExceptionOnStringNot64Length() {
        $this->setExpectedException(
                'UnexpectedValueException', 'Given string should be exactly 64 characters length. String length is: 3'
        );

        $stringReverser = new \Application\Service\StringReverser();

        $stringReverser->reverseString("abc");
    }

    public function testStringReverserShouldReverseString() {
        $stringToReverse = "stringo apvertejas. Rasom 64 simbolius. Test test test test test";
        $staticResultFromStringReverser = "tset tset tset tset tseT .suilobmis 46 mosaR .sajetrevpa ognirts";

        $stringReverser = new \Application\Service\StringReverser();

        $resultFromStringReverser = $stringReverser->reverseString($stringToReverse);

        $this->assertEquals($resultFromStringReverser, $staticResultFromStringReverser);
    }

    public function testSaveActionLoggerEntityWithEntityManagerMockReturnsTrue() {
        $soapActionLog = new \Application\Entity\soap();

        $soapActionLog->setRequest("stringo apvertejas. Rasom 64 simbolius. Test test test test test");
        $soapActionLog->setResponse("tset tset tset tset tseT .suilobmis 46 mosaR .sajetrevpa ognirts");

        $this->assertEquals($this->em->persist($soapActionLog), true);
        $this->assertEquals($this->em->flush(), true);
    }

}
