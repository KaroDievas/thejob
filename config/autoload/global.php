<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
$dbParams = array(
    'hostname' => 'localhost',
    'port' => 3306,
    'username' => 'root',
    'password' => '',
    'database' => 'zf2doctrine2'
);

return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'host' => $dbParams['hostname'],
                    'port' => $dbParams['port'],
                    'user' => $dbParams['username'],
                    'password' => $dbParams['password'],
                    'dbname' => $dbParams['database'],
                    'driverOptions' => array(
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                    ),
                )
            )
        )
    ),
    'ZendDeveloperTools' => array(
        'profiler' => array(
            'enabled' => true,
            'strict' => true,
            'verbose' => true,
            'flush_early' => false,
            'cache_dir' => 'data/cache',
            'collectors' => array(),
            'verbose_listeners' => array('application' => array(
                    'ZDT_TimeCollectorListener' => true,
                    'ZDT_MemoryCollectorListener' => true,
                ))
        ),
        'toolbar' => array(
            'enabled' => true,
            'auto_hide' => false,
            'position' => 'bottom',
            'version_check' => false,
            'entries' => array(),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);
